package huffmannb;

import java.util.PriorityQueue;
import java.util.Map;
import java.util.Comparator;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.Files;
import java.util.HashMap;
import java.io.FileWriter;
import java.io.IOException;

public class Huffman {

	private Node root;
	private byte[] mapping;
	private byte[] data;
	//private PriorityQueue<Node> queue;
	private String symbolMapHeader;
	private HashMap<Integer, String> symbolMap;

	private class Node {

		private final int key;
		private final int value;
		private final Node left;
		private final Node right;

		public Node(int key, int value) {
			this.key = key;
			this.value = value;
			this.left = null;
			this.right = null;
		}

		public Node(int key, Node left, Node right) {
			this.key = key;
			this.value = 0;
			this.left = left;
			this.right = right;
		}

		public boolean isLeaf() {
			return this.left == null && this.right == null;
		}
	}

	private class QueueSort implements Comparator<Node> {

		@Override
		public int compare(Node n1, Node n2) {
			return n1.key - n2.key;
		}
	}

	private byte[] load_file(String filename) throws IOException {
		Path path = Paths.get(filename);
		return Files.readAllBytes(path);
	}

	private void createMapHeader(Node node, String path) {
		if (node.isLeaf()) {
			String entry = node.value + ":" + path + " ";
			this.symbolMap.put(node.value, path);
			this.symbolMapHeader = this.symbolMapHeader + entry;
		} else {
			createMapHeader(node.left, path + "0");
			createMapHeader(node.right, path + "1");
		}
	}

	public void compress(String filename) throws IOException {

		// load the file into a byte array
		this.data = load_file(filename);

		// create the PriorityQueue object
		QueueSort qsort = new QueueSort();
		PriorityQueue<Node> queue = new PriorityQueue(this.data.length, qsort);
		System.out.println("Number of bytes " + this.data.length);

		// create a symbol table and count occurances of symbols
		int[] count = new int[510];
		for (int i = 0; i < this.data.length; i++) {
			++count[this.data[i] + 255];
		}

		// create and enqueue a node for all symbols present
//		System.out.println("Enqueueing....");
		int symbolCount = 0;
		for (int i = 0; i < 510; i++) {
			if (count[i] != 0) {
//				System.out.println((i-255) + " : " + count[i]);
				Node node = new Node(count[i], i - 255);
				queue.offer(node);
				++symbolCount;
			}
		}

		// create the huffman tree from the filled queue
		while (true) {
			Node left = queue.poll();
			Node right = queue.poll();
//			System.out.println(left.key + " " + right.key);
			int sum = left.key + right.key;
			Node parent = new Node(sum, left, right);
			if (queue.size() == 0) {
				this.root = parent;
				break;
			} else {
				queue.offer(parent);
			}
		}

		// symbolmap initialization and creation	
		this.symbolMap = new HashMap(symbolCount);
		this.symbolMapHeader = "";
		createMapHeader(this.root, "");
		System.out.println(this.symbolMap.size());
//		System.out.println(this.mapHeader);

		// write encoded data to a text file
		FileWriter writer = new FileWriter("compressed.txt");
		writer.write(this.symbolMapHeader);
		for (int i = 0; i < this.data.length; i++) {
			Byte b = this.data[i];
			String binaryString = this.symbolMap.get(b.intValue());
			int pos = 0;
			while(pos < binaryString.length()){
				byte nextByte = 0x00;
				for(int j=0;j<8 && pos+j < binaryString.length(); j++){
					//nextByte << 0x01;
					nextByte++;
					nextByte += binaryString.charAt(pos+i)=='0'?0x0:0x1;
				}
				writer.write(nextByte);
				pos+=8;
			}
		}

	}

	public static void decompress(String path) {

	}

//	public static void main(String[] args) {
//		if (args.length < 2) {
//			System.out.println("Please supply a run mode and filename");
//			System.exit(1);
//		} else {
//			String path = args[1];
//			Huffman H = new Huffman();
//			if (args[0] == "-c") {
//				H.compress(path);
//			} else if (args[0] == "-d") {
//				H.decompress(path);
//			} else {
//				System.out.println("Invalid Usage: -c Compress -d Decomress");
//				System.exit(1);
//			}
//		}
//
//	}
	public static void main(String[] args) throws IOException {
		Huffman H = new Huffman();
		H.compress("j:/tj/Huffman/wow.exe");
	}

}
