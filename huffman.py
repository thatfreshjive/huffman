import binascii

class LeafNode:

    def __init__(self, key, value):
        self.key = key
        self.value = value
        self.type = 'LEAF'

class BranchNode:

    def __init__(self, value, left, right):
        self.value = value
        self.left = left
        self.right = right
        self.type = 'BRANCH'

class Queue:

    def __init__(self, max_size=None):
        self.queue = []

    def pop():
        return self.queue.pop()

    def at_root():
        return len(self.queue) == 1

    def push(node):
        self.queue.append(node)



class Huffman:

    def __init__(self, filename):
        self.filename = filename
        self.symtable = {}
        self.queue = Queue()

    def load_file(self):
        with open(self.filename, 'r') as F:
            sym = binascii.hexlify(F.read(1))
            for S in sym:
                if self.symtable.haskey(S):
                    self.symtable[S] = self.symtable[S] + 1
                else:
                    self.symtable[S] = 1
        symtable['EOF'] = 1

    def load_queue():
        for i in self.symtable.values().sort():
            for k in self.symtable:
                if self.symtable[k] == i:
                    node = LeafNode(k, i)
                    queue.push(node)

    def create_bst():
        while not self.queue.at_root():
            left = self.queue.pop()
            right = self.queue.pop()
            if left.type == 'LEAF'
            sum = left.value + right.value
            parent = BranchNode(sum, left, right)
            self.queue.push(parent)



